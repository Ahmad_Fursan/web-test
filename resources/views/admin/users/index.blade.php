@extends('admin.layouts.app')

@section('content')
    <div class="card mb-3 height-auto">
        <div class="card-body p-30">
            <h3 class="card-body--title">Import users from CSV file</h3>
            <form action="" class="form-inline">
                <div class="form-group mr-2">
                    @component('components.import-csv', ['name' => 'users_list'])
                    @endcomponent
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success-2 btn-sm btn-with-icon">
                        <i class="icon-add-button-inside-black-circle"></i>
                        Add user
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="card height-auto">
        <div class="card-body p-30">
            <h3 class="card-body--title-2 mb-4">System users</h3>
            <div class="table-responsive">
                <div class="table-actions">
                    <div class="actions--wrapper">
                        <span>Sort by:</span>
                        <a href="">Users</a>
                        <a href="">Only Non-mentors</a>
                        <a href="">Only Mentors</a>
                    </div>
                </div>
                @if (count($users))
                    <table class="table table-striped table-sm custom-table">
                        <thead>
                        <tr>
                            <th width="20%" scope="col"><i class="icon-caret-square-down-regular table-icon"></i>Name
                            </th>
                            <th width="25%" scope="col"><i class="icon-caret-square-down-regular table-icon"></i>Email
                            </th>
                            <th width="10%" scope="col"><i class="icon-caret-square-down-regular table-icon"></i>Mentor
                            </th>
                            <th width="10%" class="actions" width="20%" scope="col"><i
                                        class="icon-caret-square-down-regular table-icon"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user['name'] }}</td>
                                <td>{{ $user['email'] }}</td>
                                <td>
                                    <label for="check-{{$user['name']}}" class="checkbox--wrapper">
                                        <input type="checkbox" id="check-{{$user['name']}}">
                                        <i class="icon-checkmark"></i>
                                    </label>
                                </td>
                                <td class="actions">
                                    <a href="#toggle">More</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @component('components.table-footer')
                    @endcomponent
                @else
                    <div class="alert alert-warning">There is no data</div>
                @endif
            </div>
        </div>
    </div>
    <style>

        .form-group {
            max-width: 100%;
        }

        .card.height-auto {
            height: auto;
        }

        .card-body.p-30 {
            padding: 30px;
        }

        .card-body--title {
            color: #444A49;
            font-size: 18px;
            font-weight: 500;
            margin-bottom: 18px;
        }

        .card-body--title-2 {
            color: #8F939C;
            font-size: 16px;
            text-transform: uppercase;
        }

        .btn.btn-with-icon i {
            font-size: 13px;
        }

        .btn.btn-success-2 {
            background-color: #09C796;
            border: 1px solid #09C796;
            padding-left: 18px;
            padding-right: 18px;
            color: #fff;
        }

        .btn.btn-gray {
            background-color: #444A49;
            color: #fff;
        }

        .btn.btn-success-2:hover {
            background-color: #098864;
        }

        i.table-icon {
            margin-right: 5px;
            font-size: 13px;
            font-weight: bold;
        }

        .table-responsive {
            width: auto;
            display: inline-flex;
            flex-direction: column;
            max-width: 100%;
        }

        .custom-table {
            width: auto;
            max-width: 100%;
            margin-bottom: 0;
            overflow-x: auto;
            display: inline-block;
        }

        .custom-table th {
            color: #444A49;
        }

        .custom-table th i.table-icon {
            color: #000;
        }

        .table-actions {
            display: flex;
            justify-content: flex-end;
            padding: 10px 0;
        }

        .actions--wrapper {
            display: flex;
            flex-wrap: wrap;
        }

        .actions--wrapper a {
            margin-right: 13px;
            text-decoration: underline;
        }

        .actions--wrapper a:last-child {
            margin-right: 0;
        }

        .actions--wrapper a,
        .actions--wrapper span {
            font-size: 12px;
            font-weight: 500;
        }

        .actions--wrapper span {
            margin-right: 5px;
        }

        .custom-table tbody tr:nth-child(odd) {
            background-color: #EFF5F9;
        }

        .custom-table tbody tr:nth-child(even) {
            background-color: #F9FAFA;
        }

        .custom-table.table-sm td {
            padding: 0.27rem;
            font-size: 14px;
        }

        .checkbox--wrapper {
            display: inline-flex;
            align-items: center;
            cursor: pointer;
        }

        .form-group .checkbox--wrapper {
            margin-bottom: 0;
        }

        .checkbox--wrapper input {
            display: none;
        }

        .checkbox--wrapper i,
        .checkbox--wrapper span {
            font-size: 15px;
            color: #444A49;
            transition: all 0.2s linear;
        }

        .checkbox--wrapper span {
            margin-left: 5px;
        }

        .checkbox--wrapper i {
            border: 2px solid #444A49;
            border-radius: 5px;
            color: transparent;
            padding: 2px;
        }

        .checkbox--wrapper input:checked + i {
            color: #444A49;
        }

        .custom-table td.actions {
            color: #444A49;
            text-decoration: underline;
        }

        .custom-table th.actions {
            padding: 0.3rem 0.8rem;
        }

        .custom-table.table-sm td.actions {
            padding: 0.57rem 0.8rem;
        }

        .custom-table.table-sm th {
            font-weight: normal;
            font-size: 13px;
            padding: 0.62rem;
        }
    </style>
@endsection
