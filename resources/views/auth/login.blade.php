@extends('layouts.plain')

@section('content')
    <div class="card custom-card">
        <div class="card-body">
            <div class="card-image">
                <img src="{{ asset('assets/emerj-logo.png') }}" alt="{{ __('project') }}">
            </div>
            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                <div class="form-group">
                    <input id="email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" value="{{ old('email') }}" autofocus
                           placeholder="{{ __('E-Mail Address') }}" aria-describedby="inputGroupPrepend">
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" placeholder="{{ __('Password') }}">
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="remember" class="checkbox--wrapper">
                        <input type="checkbox" id="remember" {{ old('remember') ? 'checked' : '' }}>
                        <i class="icon-checkmark"></i>
                        <span>{{ __('Remember Me') }}</span>
                    </label>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success-2 full">
                        {{ __('Login') }}
                    </button>
                </div>
                <div class="d-flex justify-content-end">
                    <a class="btn btn-link btn-link-2" href="{{ route('password.request') }}">
                        {{ __('Forgot Password?') }}
                    </a>
                </div>
            </form>
        </div>
    </div>
@endsection
<style>
    .card.custom-card {
        border-radius: 16px;
        box-shadow: 0 0 23px 3px rgba(68, 74, 73, 0.2);
        width: 440px;
        max-width: 100%;
        border: 0;
    }

    .card-image {
        text-align: center;
        margin-bottom: 35px;
    }

    .card.custom-card .card-body {
        padding: 30px 55px;
    }

    .checkbox--wrapper {
        display: inline-flex;
        align-items: center;
        cursor: pointer;
    }

    .form-group .checkbox--wrapper {
        margin-bottom: 0;
    }

    .checkbox--wrapper input {
        display: none;
    }

    .checkbox--wrapper i,
    .checkbox--wrapper span {
        font-size: 15px;
        color: #444A49;
        transition: all 0.2s linear;
    }

    .checkbox--wrapper span {
        margin-left: 5px;
    }

    .checkbox--wrapper i {
        border: 2px solid #444A49;
        border-radius: 5px;
        color: transparent;
        padding: 2px;
    }

    .checkbox--wrapper input:checked + i {
        color: #444A49;
    }

    .btn.btn-success-2 {
        background-color: #09C796;
        border: 1px solid #09C796;
        padding-left: 18px;
        padding-right: 18px;
        color: #fff;
    }

    .btn.btn-success-2:hover {
        background-color: #098864;
    }

    .form-group .btn.full {
        width: 100%;
    }

    .btn.btn-link-2 {
        color: #7D7D7D;
        font-size: 14px;
        padding: 0;
    }

    @media (max-width: 440px) {
        .card.custom-card .card-body {
            padding: 15px;
        }
    }
</style>
