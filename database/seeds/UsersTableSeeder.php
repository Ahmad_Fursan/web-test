<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 1)->create(['email' => 'admin@example.com']);
        factory(App\Models\User::class, 1)->create(['email' => 'mentor@example.com']);
        factory(App\Models\User::class, 1)->create(['email' => 'mentee@example.com']);
        factory(App\Models\User::class, 10)->create();
    }
}
