<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            if(auth()->check()) {
                $auth = true;
                $guest = false;
                $user = auth()->user();
            }
            else {
                $auth = false;
                $guest = true;
                $user = false;
            }
            View::share('auth_user', $user);
            View::share('auth', $auth);
            View::share('guest', $guest);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
