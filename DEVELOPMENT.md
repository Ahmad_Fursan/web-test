## Development guidelines

We use feature branching / git flow to work on this project.

### Workflow
- Create a feature branch

`git checkout -b feature/MYFEATURE develop`

- Commit your changes (whenever you have made some work on a subtask)

`$ git commit -m "task description"`

- Push your changes (preferably daily)

`$ git push origin feature/MYFEATURE`

- When you're finished, kindly create a Pull Request (using BitBucket web interface), 
to allow code reviews and internal testing, kindly note any feedback/needed tweaks.

- Once code review is done, your branch will be merged, good job :+1

- Get back to develop branch to start attacking the next task ^_^

`$ git checkout develop`
