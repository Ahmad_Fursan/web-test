# EmerjWork Web project
 
This repository will contain web project source code for EmerjWork application.

To run this locally, go to Setup section

## Requirements

### For Development:
- [PHP 7.2](https://secure.php.net/)
- [Composer](https://getcomposer.org/)
- [Nodejs + NPM](https://nodejs.org/)

### For Production
- [PHP 7.2](https://secure.php.net/)
- [Composer](https://getcomposer.org/)
- [MySQL 8.0 +](https://dev.mysql.com/downloads/mysql/)
- [Apache2.4 +](https://httpd.apache.org/)

## Setting up environment

- Clone this repository  
`$ git clone git@bitbucket.org:emerjteam/web.git`
- Download PHP packages (dependencies)  
`$ composer install -vvv`
- Create an Application key:  
`$ php artisan key:generate`
- Copy .env.example file to .env  
`$ cp .env.example .env`
- Create and empty file (for SQLite database):  
    On Windows: `$ type nul > database/database.sqlite`
    On Linux/Mac: `$ touch database/database.sqlite`
- Create the database tables  
`$ php artisan migrate`
- Optional: create dummy data  
`$ php artisan db:seed`
- Run the Webserver  
`$ php artisan serve` 
- Go to the site:  
    [http://127.0.0.1:8000](http://127.0.0.1:8000)

## Credentials:

*If* you want to use dummy credentials (for development only), use:

For and admin user: admin@example.xyz  
For and mentor user: mentor@example.xyz  
For and mentee user: mentee@example.xyz  

With passwords for any: secret

## Development Guidelines

Checkout DEVELOPMENT.md for documentation

## Deployment Guidelines

Deployment is exactly the same as setting up for any environment, except we modify .env file:
- APP_ENV=production  
- APP_DEBUG=false  
- Switch to MySQL database! create a database and configure it's parameters in .env file
