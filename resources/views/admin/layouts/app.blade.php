<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>EmerjWork Admin | @yield('title')</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{asset('css/layout.css')}}" rel="stylesheet">
    <link href="{{asset('css/dashboard.css')}}" rel="stylesheet">
    <link href="{{asset('assets/fonts/icomoon/style.css')}}" rel="stylesheet">

</head>

<body>

<div id="wrapper" class="toggled">
    <!-- User profile menu-->
@include('admin/layouts/user_menu')

<!-- Sidebar -->
    <nav id="sidebar-wrapper" class="sidebar-wrapper">
        <img class="your-logo" src="{{url('/assets/your-logo.png')}}" alt="Italian Trulli">
        <ul class="sidebar-nav">
            <li>
                <a class="active" href="{{url('/admin')}}">
                    <img src="{{url('/assets/graph.png')}}" alt="Dashboard">
                    <span>Dashboard</span>
                </a>
            </li>
            <li>
                <a href="{{url('/admin/users')}}">
                    <img src="{{asset('/assets/user-female.png')}}" alt="users">
                    <span>Users</span>
                </a>
            </li>
            <li>
                <a href="{{url('/admin/tags')}}">
                    <img src="{{asset('/assets/tags.png')}}" alt="tags">
                    <span>Tags</span>
                </a>
            </li>
        </ul>
    </nav>
    <button class="navbar-toggler navbar-light" type="button" data-toggle="sidebar-wrapper" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div class="dashboard-view">
        @yield('content')
    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.js"></script>

<!-- Menu Toggle Script -->
<script>
    $(".navbar-toggler").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>
</html>
