@extends('admin.layouts.app')

@section('content')

    $users_registered {{ $users_registered}} <br />
    $mentors_registered {{ $mentors_registered}} <br />
    $average_rating {{ d($average_rating) }} <br />
    $connections_made {{ d($connections_made) }} <br />
    $mentors_per_tag {{ d($mentors_per_tag) }} <br />
    $trending_tags {{ d($trending_tags)}} <br />
    $users_by_department {{ d($users_by_department) }} <br />
    $mentors_leader_boards {{ d($mentors_leader_boards)}} <br />
    $users_by_location {{ d($users_by_location)}} <br />

    <div class="row">
        <div class="col-md-12 col-lg-6">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="oval">
                                    <img src="./assets/user-female.png" alt="Italian Trulli">
                                </div>
                                <div class="media-body">
                                    <div class="h1">1</div>
                                    <h4>Users registered</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="oval">
                                    <img src="./assets/user-female.png" alt="Italian Trulli">
                                </div>
                                <div class="media-body">
                                    <div class="h1">2</div>
                                    <h4>Mentors registered</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="row">
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="oval">
                                    <img src="./assets/user-female.png" alt="Italian Trulli">
                                </div>
                                <div class="media-body">
                                    <div class="h1">90% <span class="note">Matches</span></div>
                                    <h4>20 connections made</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card">
                        <div class="card-body">
                            <div class="media d-flex">
                                <div class="oval">
                                    <img src="./assets/user-female.png" alt="Italian Trulli">
                                </div>
                                <div class="media-body">
                                    <div class="h1">4</div>
                                    <h4>Average rating</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-top">
        <div class="col-md-12 col-lg-6">
            <div class="card statistics-card">
                <div class="card-body">
                    <div class="media d-flex">
                        <div class="media-body">
                            <h4>Mentors per Tag</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="card statistics-card">
                <div class="card-body">
                    <div class="media d-flex">
                        <div class="media-body">
                            <h4>Trending Tags</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-top">
        <div class="col-md-12 col-lg-6">
            <div class="card statistics-card">
                <div class="card-body">
                    <div class="media d-flex">
                        <div class="media-body">
                            <h4>Users by Location</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="card statistics-card">
                <div class="card-body">
                    <div class="media d-flex">
                        <div class="media-body">
                            <h4>Mentor’s Leader Board</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row padding-top">
        <div class="col">
            <div class="card map-card">
                <div class="card-body">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2646.216225729443!2d-89.2391164!3d48.45238070000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4d5921616d61c26b%3A0x15e5407d2071c8dd!2s109+Hogarth+St%2C+Thunder+Bay%2C+ON+P7A+7G8!5e0!3m2!1sen!2sca!4v1424371524427" width="100%" height="450" frameborder="0" style="border:0"></iframe>
                </div>
            </div>
        </div>
    </div>
@endsection

