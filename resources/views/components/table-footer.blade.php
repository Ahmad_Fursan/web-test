<div class="table-footer">
    <ul class="pagination">
        <li><a href="" class="active">1</a></li>
        <li><a href="">2</a></li>
        <li><a href="">3</a></li>
    </ul>
    <div class="showing">
        <span>Show</span>
        <ul>
            <li><a href="" class="active">10</a></li>
            <li><a href="">50</a></li>
            <li><a href="">All</a></li>
        </ul>
    </div>
</div>

<style>
    .table-footer {
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 10px 0;
    }

    .table-footer .pagination {
        display: flex;
        align-items: center;
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    .table-footer .pagination li a {
        color: #444A49;
        text-decoration: underline;
        font-size: 13px;
        margin-right: 10px;
    }

    .table-footer .pagination li:last-child a {
        margin-right: 0;
    }

    .table-footer .pagination li a.active {
        color: #2DCFA5;
        text-decoration: none;
    }

    .table-footer .showing {
        display: flex;
        align-items: center;
    }

    .table-footer .showing > span {
        color: #444A49;
        font-size: 13px;
        margin-right: 5px;
    }

    .table-footer .showing > ul {
        display: flex;
        align-items: center;
        list-style-type: none;
        padding: 0;
        margin: 0;
    }

    .table-footer .showing > ul a {
        color: #444A49;
        text-decoration: underline;
        font-size: 13px;
        margin-right: 10px;
    }

    .table-footer .showing > ul li:last-child a {
        margin-right: 0;
    }

    .table-footer .showing > ul a.active {
        color: #2DCFA5;
        text-decoration: none;
    }
</style>