<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users_registered = 281;
        $mentors_registered = 9;
        $average_rating = [2, 3];
        $connections_made = [90, 20];
        $mentors_per_tag = [
            'Sexual Harassment' => 20,
            'Life Work Balance' => 41,
            'Salary Negotiation' => 35,
        ];
        $trending_tags = [
            'Relocation' => 50,
            'Conflicts Promotion' => 20
        ];
        $users_by_department = [
            'Marketing' => 40,
            'Sales' => 20,
            'IT' => 55
        ];

        $mentors_leader_boards = [
            1 => ['name' => 'Tom Burns', 'Rating' => 5, 'Session' => 14],
            2 => ['name' => 'Myra Cannon', 'Rating' => 4.7, 'Sessions' => 26],
            3 => ['name' => 'Leila Bates', 'Rating' => 3.6, 'Sessions' => 24],
        ];

        $users_by_location = [
            'Israel' => 200,
            'UAE' => 150,
        ];




        return view('admin/home/index',
            compact('users_registered','mentors_registered', 'average_rating', 'connections_made',
                'mentors_per_tag', 'trending_tags', 'users_by_department', 'mentors_leader_boards', 'users_by_location')
        );
    }
}
