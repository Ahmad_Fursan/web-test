<div class="file-input">
    CSV file
    <div class="file-btn btn btn-gray btn-sm">
        Import
    </div>
    <input type="file" name="{{$name}}">
</div>

<style>
    .file-input .btn {
        padding-left: 23px;
        padding-right: 23px;
    }

    .file-input {
        border: 1px solid #979797;
        width: 330px;
        max-width: 100%;
        height: 40px;
        background-color: #fff;
        padding: 5px;
        padding-left: 11px;
        color: #A3A6AE;
        font-size: 14px;
        display: flex;
        align-items: center;
        justify-content: space-between;
        position: relative;
    }

    .file-input input[type="file"] {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: 0;
        opacity: 0;
        cursor: pointer;
    }
</style>