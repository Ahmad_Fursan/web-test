<?php


use Illuminate\Support\Debug\Dumper;

function d(...$args) {
    {
        foreach ($args as $x) {
            (new Dumper)->dump($x);
        }
    }

}